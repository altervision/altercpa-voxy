#!/bin/bash

# Usage:
# wget wget https://gitlab.com/altervision/altercpa-voxy/-/raw/main/setup-ru.sh
# bash setup-ru.sh <telegram-bot-token>

# Get the bot token
if [ -z "$1" ]; then
	echo "No bot token specified"
	exit 1
else
	BOTTOKEN="$1"
fi

# Install basic soft
apt update && apt upgrade -y
apt install -y python3 python3-dev python3-pip unzip ffmpeg

# Install Python modules
pip3 install PyTelegramBotAPI
pip3 install vosk
pip3 install wave
pip3 install numpy
pip3 install torch
pip3 cache purge

# Download
mkdir /home/ml
cd /home/ml
wget https://alphacephei.com/vosk/models/vosk-model-ru-0.22.zip
unzip vosk-model-ru-0.22.zip
rm -f vosk-model-ru-0.22.zip
wget https://models.silero.ai/te_models/v2_4lang_q.pt

# Download the bot
mkdir /home/bot
cd /home/bot
wget https://gitlab.com/altervision/altercpa-voxy/-/raw/main/voxy.py
chmod a+x voxy.py

# Set the language and token
sed -i "s/YOURBOTTOKEN/$BOTTOKEN/g" voxy.py
#sed -i "s|\'ru\'|'en'|g" voxy.py
#sed -i "s|vosk-model-ru|vosk-model-en|g" voxy.py

# Start the service
wget https://gitlab.com/altervision/altercpa-voxy/-/raw/main/voxybot.service -O /lib/systemd/system/voxybot.service
systemctl daemon-reload
systemctl enable voxybot.service
service voxybot start