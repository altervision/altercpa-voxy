# AlterCPA Voxy Bot

Voxy is simple voice recognition bot for Telegram. It uses local recognition by vosk and text optimisation by Silero.

Voxy - это простой бот распознавания голоса для Telegram. Он использует локальное распознование от vosk и оптимизацию текста от Silero.

## Installation in English

Run the following commands to install `voxy` in English. Replace `YOURBOTTOKEN` with your bot token.

```sh
wget https://gitlab.com/altervision/altercpa-voxy/-/raw/main/setup-en.sh
bash setup-en.sh YOURBOTTOKEN
```

Manual instructions are [here in our blog](https://en.altercpa.pro/2021/12/11/secure-voice-recognition-telegram/)

## Установка на русском

Выполните эти команды для установки `voxy` на русском. Вместо `YOURBOTTOKEN` укажите ваш токен бота.

```sh
wget https://gitlab.com/altervision/altercpa-voxy/-/raw/main/setup-ru.sh
bash setup-ru.sh YOURBOTTOKEN
```

Инструкция по ручной установке [здесь, в нашем блоге](https://ru.altercpa.pro/2021/12/11/secure-voice-recognition-telegram/)

