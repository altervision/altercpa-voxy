#!/usr/bin/python3
# coding: utf-8


# Preloading
import telebot
import pathlib
import requests
import subprocess
import os
import json
import wave
import torch
from vosk import Model, KaldiRecognizer


# Your Telegram bot token from @BotFather
TOKEN = 'YOURBOTTOKEN'

# Path to vosk model
MODEL = r"/home/ml/vosk-model-ru-0.22"

# Path to Silero text enchancer and it's language
TEMODEL = "/home/ml/v2_4lang_q.pt"
LANG = 'ru'


# Initialize
WORKDIR = str(pathlib.Path(__file__).parent.absolute())
bot = telebot.TeleBot( TOKEN )
model = Model( MODEL )
voska = KaldiRecognizer( model, 16000 )
if LANG:
    tmodel = torch.package.PackageImporter( TEMODEL ).load_pickle( "te_model", "model" )


# Handle voice messages
@bot.message_handler(content_types=["voice","video_note"])
def voice_decoder(message):

    # Get the message
    if ( message.voice != None ):
        file =  message.voice
    elif ( message.video_note != None ):
        file =  message.video_note
    else:
        return False

    # Download the file
    finfo = bot.get_file(file.file_id)
    try:
        contents = requests.get( 'https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, finfo.file_path) )
    except Exception:
        return False

    # Save the file
    downpath = WORKDIR + "/" + file.file_unique_id
    with open( downpath, 'wb' ) as dest:
        dest.write(contents.content)

    # Convert audio
    path = audioconvert( downpath )
    if ( path == False ):
        return False

    # Get the text
    text = speech2text( path )
    os.remove( path )
    if ( text == False or text == "" or text == " " ):
        return False
    elif LANG:
        text = tmodel.enhance_text( text, LANG )

    # Reply to the message
    bot.reply_to(message, text)


# Convert audio file
def audioconvert(path):

    # Prepare the conversion process
    out_path = path + ".wav"
    command = [
        r'/usr/bin/ffmpeg',
        '-i', path,
        '-acodec', 'pcm_s16le',
        '-ac', '1',
        '-ar', '16000',
        out_path
    ]

    # Run the conversion and remove the file
    result = subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL )
    os.remove( path )

    # Check the result
    if ( result.returncode ):
        os.remove( out_path )
        return False
    else:
        return out_path


# Speech to text
def speech2text(path):
    wf = wave.open(path, "rb")
    result = ''
    last_n = False
    while True:
        data = wf.readframes(16000)
        if len(data) == 0:
            break
        if voska.AcceptWaveform(data):
            res = json.loads(voska.Result())
            if res['text'] != '':
                result += f" {res['text']}"
                last_n = False
            elif not last_n:
                result += '\n'
                last_n = True
    res = json.loads(voska.FinalResult())
    result += f" {res['text']}"
    return result


# Run it!
if __name__ == '__main__':
    bot.infinity_polling()
